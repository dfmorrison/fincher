(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:alexandria :iterate :cl-ppcre :sqlite
                  :bordeaux-threads :uiop :cl-threadpool :trivial-garbage :verbose)
                :silent (not *load-verbose*)))

(defpackage :fincher
  (:use common-lisp :alexandria :iterate)
  (:local-nicknames (:s :sqlite) (:u :uiop) (:tp :cl-threadpool))
  (:export #:run-simulations))

(in-package :fincher)

(load (merge-pathnames "act-up-v1_3_0" *load-pathname*))

(setf tp:*logger* (lambda (level who fmt args)
                    (if (and (eq who :cl-threadpool) (eq level :info))
                        (apply #'v:debug who fmt args)
                        (apply #'v:log level who fmt args))))
(v:output-here)                         ; in case we're running under SLIME

(defparameter +default-root+ #P"/home/mkm/cp100/")
(defparameter +default-simulation-limit+ nil)
(defparameter +default-thread-count+ 4)
(defparameter +fast-directory+ #P"/dev/shm/")



;; Cobble around a memory leak in ACT-UP

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; Suppress the redefined warning
  (fmakunbound 'create-chunk))

(defvar *chunk-count*)                  ; needs to be bound before creating any chunks!

(defun create-chunk (description &optional (memory *memory*) (trace *verbose*))
  (let ((name (if (symbolp description) description (format nil "MEMORY~D" (incf *chunk-count*))))
        (content (if (symbolp description) nil description)))
    (when trace (format t "Creating Chunk ~A Content ~S.~%" name content))
    (setf (gethash name memory) (make-chunk :name name :content content :creation (get-time)
                                            :references (if *optimized-learning* 1 (list (get-time)))))))



(proclaim '(ftype function print-node)) ; make SBCL shut up about the forward reference

(defstruct (node (:print-object print-node))
  (id nil :read-only t)
  (neighbors nil)
  (memory (prog1 (init-memory) (setf *memory* nil)))
  pending)

(defun print-node (node &optional (stream *standard-output*))
  (print-unreadable-object (node stream :type t :identity t)
    (format stream "~D (~D)" (node-id node) (length (node-neighbors node)))))



(defun keywordize (string)
  (make-keyword (string-upcase (substitute #\- #\_ string))))

(defun read-config (db)
  (iter (with result := (make-hash-table :test 'eq))
        (for (param value) :in-sqlite-query "SELECT parameter, value FROM config"
                           :on-database db)
        (setf (gethash (keywordize param) result) value)
        (finally (return result))))

(defvar *config*)

(defun get-config (key &optional (type :string))
  (when-let ((value (gethash key *config*)))
    (ecase type
      (:string value)
      (:keyword (keywordize value))
      (:boolean (cond ((string-equal value "TRUE") t)
                      ((string-equal value "FALSE") nil)
                      (t (error "Can't interpret ~S as a Boolean" value))))
      (:integer (parse-integer value))
      (:float (coerce (read-from-string value) 'float)))))

(defun set-memory-parameters ()
  (iter (for (key param type) :in '((:bll :decay :float)
                                    (:mp :mismatch :float)
                                    (:ans :noise :float)
                                    (:rt :threshold :float)
                                    (:tmp :temperature :float)
                                    (:ol :optimized-learning :boolean)))
        (parameter key (get-config param type))))



(defun make-network (db)
  (iter (with nodes := (make-array (1+ (s:execute-single db "SELECT MAX(name) FROM node"))
                                   :initial-element nil))
        (for (id) :in-sqlite-query"SELECT name FROM node" :on-database db)
        (setf (aref nodes id) (make-node :id id))
        (finally (iter (for (src tgt) :in-sqlite-query "SELECT source, target FROM edge"
                                      :on-database db)
                       (push (aref nodes tgt) (node-neighbors (aref nodes src))))
                 (return nodes))))

(defvar *nodes*)

(defun learn-initial-history (db)
  (iter (for (id *time* att) :in-sqlite-query "SELECT name, timestep, narrative FROM history ORDER by timestep"
                             :on-database db)
        (learn `((attitude ,att)) :memory (node-memory (aref *nodes* id))))
  (actr-time 1))

(defmacro with-database ((var path) &body body)
  `(%with-database ,path (lambda (,var) ,@body)))

(defun %with-database (path thunk)
  (s:with-open-database (db path)
    (s:execute-non-query db "PRAGMA JOURNAL_MODE=OFF")
    (let* ((*memory* nil)               ; Need to bind *memory* and *time* to make
           (*time* 0.0)                 ;   ACT-UP thread safe.
           (*chunk-count* 0)            ; See top of this file for this kludge.
           (*config* (read-config db))
           (*nodes* (make-network db)))
      (learn-initial-history db)
      (setf *nodes* (delete nil (coerce *nodes* 'list)))
      (funcall thunk db))))

;; Note weird word usage in this project. Both attitude and narrative are the same thing,
;; just real numbers in the range [-1, 1], but in the database we use narrative for the
;; usual blended value and attitude for the blended value we get with zero activation
;; noise.

;; We could make this more efficient by breaking open the activation computaton so we only
;; have to compute the noise-free base level activation once rather than twice (once for
;; narrative and once for attitude).

(defun blend-and-variance (memory)
  (let* ((weights (iter (for (nil chunk) :in-hashtable memory)
                        (collect (cons (second (assoc 'attitude (chunk-content chunk)))
                                       (exp (/ (activation chunk) *temperature*))))))
         (weight-sum (reduce #'+ weights :key #'cdr :initial-value 0))
         (mean (/ (reduce #'+ (mapcar (lambda (x)
                                        (* (car x) (cdr x)))
                                      weights))
                  weight-sum)))
    (values mean
            (when (> (hash-table-count memory) 1)
              (/ (reduce #'+ (mapcar (lambda (x)
                                       (* (expt (- (car x) mean) 2) (cdr x)))
                                     weights))
                 (- weight-sum 1))))))

(defun execute-simulation (path)
  (with-database (db path)
    (s:execute-non-query db "ALTER TABLE history ADD COLUMN narrative_variance REAL")
    (s:execute-non-query db "ALTER TABLE history ADD COLUMN attitude REAL")
    (s:execute-non-query db "ALTER TABLE history ADD COLUMN attitude_variance REAL")
    (iter (for round :from 1 :to (get-config :duration :integer))
          (v:debug :fincher "Round ~D in ~A" round path)
          (dolist (n *nodes*)
            (multiple-value-bind (narr nvar)
                (blend-and-variance (node-memory n))
              (let ((*noise* 0.0))
                (multiple-value-bind (att avar)
                    (blend-and-variance (node-memory n))
                  (s:execute-non-query db "INSERT INTO history VALUES (?, ?, ?, ?, ?, ?)"
                                       (node-id n) *time* narr nvar att avar)))
              (setf (node-pending n) narr)))
          (dolist (n *nodes*)
            (labels ((learn-attitude (tgt)
                       (learn `((attitude ,(node-pending n))) :memory (node-memory tgt))))
              (learn-attitude n)
              (dolist (other (node-neighbors n))
                (learn-attitude other))))
          (actr-time 1))))



(defparameter +input-subdir+ #P"input/")
(defparameter +output-subdir+ #P"output/")
(defparameter +db-filetype+ "sqlite3")

(defun run-one-simulation (name &optional (root +default-root+))
  (let* ((file (merge-pathnames name (make-pathname :type +db-filetype+)))
         (in (merge-pathnames file (merge-pathnames +input-subdir+ root)))
         (fast (merge-pathnames file +fast-directory+))
         (out (merge-pathnames file (merge-pathnames +output-subdir+ root))))
    (unwind-protect
         (progn
           (u:run-program (format nil "/bin/cp ~A ~A" in fast))
           (execute-simulation fast)
           (u:run-program (format nil "/bin/mv ~A ~A" fast out)))
      (u:delete-file-if-exists fast))))

(defun core-count ()
  ;; Returns nil if it can't figure it out. Currently probably only works on Linux.
  (values
   (ignore-errors
    (ppcre:register-groups-bind (s)
        ("cpu cores\\s+:\\s*(\\d+)\\s*$" (u:run-program "grep 'cpu cores' /proc/cpuinfo" :output :string))
      (when s
        (parse-integer s))))))

(defparameter *default-thread-count* (or (core-count) +default-thread-count+))

(defun run-simulations (&key (root +default-root+) (workers *default-thread-count*))
  (setf root (u:ensure-directory-pathname root))
  (unless (and (integerp workers) (> workers 0))
    (setf workers 1))
  (let ((threadpool (tp:make-threadpool workers :name "Fincher")))
    (v:info :fincher "~D workers" workers)
    (unwind-protect
         (iter (with names := (iter (with out-dir := (merge-pathnames +output-subdir+ root))
                                    (with result := (make-array 1500
                                                                :adjustable t
                                                                :fill-pointer 0))
                                    (for f :in (u:directory-files (merge-pathnames +input-subdir+
                                                                                   root)))
                                    (when (string= (pathname-type f) +db-filetype+)
                                      (if (u:file-exists-p (merge-pathnames (make-pathname :name (pathname-name f)
                                                                                           :type +db-filetype+)
                                                                            out-dir))
                                          (counting t :into skipping)
                                          (vector-push-extend (pathname-name f) result)))
                                    (finally (when (> skipping 0)
                                               (v:info :fincher
                                                       "Skipping ~D databases because they appear to have been simulated already"
                                                       skipping))
                                             (return result))))
               (with i := 0)
               (with lock := (bt:make-lock "SIMULATION COUNT"))
               (with n := (length names))
               (for start :from 0 :below n :by workers)
               #-sbcl (v:warn :fincher "Random number generation is being shared between threads")
               (tg:gc :full t) ; clear out stuff a generational GC might mistakenly think is permanent
               (tp:run-jobs threadpool
                            (iter (for name :in-vector names
                                            :from start
                                            :below (min (+ start workers) n))
                                  (collect (let ((name name))
                                             ;; work around iterate's (or loop's) unfortunate scoping rules 
                                             (lambda ()
                                               #+sbcl (sb-ext:seed-random-state)
                                               (bt:with-lock-held (lock)
                                                 (v:info :fincher "Simulating ~A (~D of ~D)"
                                                         name (incf i) n))
                                               (handler-case
                                                   (run-one-simulation name root)
                                                 (error (e)
                                                   (v:error :fincher "Error simulating ~A: ~S"
                                                            name e)))))))))
      (tp:stop threadpool))))
